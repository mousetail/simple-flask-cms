document.querySelector('#create-page-button').addEventListener(
    'click',
    (ev) => {
        const input = document.querySelector('input#page-input');
        window.location = ev.currentTarget.dataset.url_root + input.value;
    }
)