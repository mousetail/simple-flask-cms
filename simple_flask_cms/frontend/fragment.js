import SimpleMDE from 'simplemde';
import {setUpImageEditor} from "./base_editor";


const simpleMDE = new SimpleMDE({element: document.getElementById("editor-main")});
setUpImageEditor(simpleMDE);

document.querySelector('#submit-button').addEventListener('click', async (ev) => {
    await fetch(
        window.location,
        {
            method: 'POST',
            body: JSON.stringify(
                {
                    content: simpleMDE.value()
                }
            ),
            headers: {
                'Content-Type': 'Application/JSON'
            }
        }
    )
})