import SimpleMDE from 'simplemde';
import {setUpImageEditor} from "./base_editor";

const simpleMDE = new SimpleMDE({element: document.getElementById("editor-main")});


document.querySelectorAll('.add-to-post-button').forEach(
    i => i.addEventListener('click', (ev) => {
            simpleMDE.value(
                simpleMDE.value() + `\n\n![${ev.target.dataset.filename}](${ev.target.dataset.link})`
            )
        }
    )
);

function submit() {
    fetch('',
        {
            'method': 'POST',
            'credentials': 'same-origin',
            'body': JSON.stringify({
                'title': document.getElementById('input-title').value,
                'content': simpleMDE.value(),
                'sort_order': document.getElementById('input-sort-order').value,
                'nav_title': document.getElementById('input-nav-title').value
            }),
            'headers': {
                'Content-Type': 'Application/JSON'
            }
        }
    )
}

document.querySelector('#submit-button').addEventListener(
    'click',
    () => submit()
)

setUpImageEditor(simpleMDE);

document.querySelector('#delete-button').addEventListener(
    'click',
    (i) => {
        console.log("button");
        document.querySelector(
            '.modal-container'
        ).style.visibility = 'unset';
    }
);

function hideModal() {
    document.querySelector('.modal-container').style.visibility = 'hidden';
}

document.querySelector('.modal-container').addEventListener(
    'click',
    i => {
        /* Check if the event hasn't bubbles first */
        if (i.target === i.currentTarget) {
            hideModal();
        }
    }
);

document.querySelector('#hide-modal-button').addEventListener(
    'click', hideModal
);

document.querySelector('#modal-delete-button').addEventListener(
    'click', async (event) => {
        let response = await fetch(event.currentTarget.dataset.url,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'Application/JSON'
                }
            });
        if (response.ok) {
            window.location = '/'
        }
    }
)

// Autosave every 30 seconds
setInterval(submit, 30000)