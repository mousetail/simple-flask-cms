async function delete_image(filename, div, link) {
    const response = await fetch(
        link,
        {
            "method": "POST"
        }
    );
    if (!response.ok) {
        alert(await response.text());
    }
    div.parentElement.removeChild(div);
}


export function setUpImageEditor(simpleMDE) {
    const files = [];

    const inputImageUpload = document.querySelector('#input-image-upload');

    document.querySelectorAll('.delete-image-button').forEach(
        i => i.addEventListener('click', async (ev) => {
            await delete_image(ev.currentTarget.dataset.filename, ev.currentTarget.parentElement,
                ev.currentTarget.dataset.delete_url
            )
        })
    )

    inputImageUpload.addEventListener('change', async (ev) => {
        const file = ev.target.files[0];
        files.push(file);

        const div = document.createElement('div');
        div.classList.add('image-upload-container');

        const image = document.createElement('img');
        image.src = URL.createObjectURL(file);
        div.appendChild(image);

        const progress_indicator = document.createElement('div');
        progress_indicator.textContent = 'uploading...';
        div.appendChild(progress_indicator);


        document.querySelector('.images-container').appendChild(
            div
        );

        const response = await fetch(
            inputImageUpload.dataset.upload_to,
            {
                'method': 'POST',
                'body': file,
                headers: {
                    'X-Page': inputImageUpload.dataset.url,
                    'X-filename': file.name
                }
            }
        )

        if (!response.ok) {
            alert(await response.text())
        }

        const data = await response.json();

        const add_button = document.createElement('button');
        add_button.textContent = 'Add to post';
        add_button.dataset.link = data.path;
        div.appendChild(add_button);

        const delete_button = document.createElement('button');
        delete_button.textContent = 'Delete';
        delete_button.dataset.delete_link = ev.target.dataset.delete_link;
        delete_button.classList.add('button-red', 'button');
        div.appendChild(delete_button);

        progress_indicator.textContent = 'Upload Completed!';
        add_button.classList.add('button-blue', 'button')

        add_button.addEventListener('click', () => {
            simpleMDE.value(
                simpleMDE.value() + `\n\n![${file.name}](${data.path})\n`
            )
        });

        delete_button.addEventListener(
            'click',
            () => {
                delete_image(
                    data.path,
                    div,
                    ev.target.dataset.delete_link + data.filename
                )
            }
        );
    })
}