import json

import flask
from flask import Flask, redirect, url_for
import simple_flask_cms

app = Flask(__name__)


@app.route('/')
def hello_world():  # put application's code here
    return flask.render_template(
        'home.html'
    )


def authentication_function(action, parameters):
    if action in simple_flask_cms.config.viewer_paths:
        return None

    auth = flask.request.authorization
    if auth and auth.username == config["username"] and auth["password"] == config["password"]:
        return None
    else:
        return flask.Response(
            status=401,
            headers={'WWW-Authenticate': 'Basic realm="FlaskCMS"'}
        )


app.register_blueprint(simple_flask_cms.cms)
with open("config.json") as f:
    config = json.load(f)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
simple_flask_cms.config.db_connection = simple_flask_cms.database_providers.sql_database.SQLDatabaseProvider(
    app
)
simple_flask_cms.config.template_name = 'page.html'
simple_flask_cms.config.authentication_function = authentication_function
simple_flask_cms.config.fragments = [
    simple_flask_cms.dataclasses.FragmentType(
        name="Home Column 1",
        description="Displayed in the left column in the home page",
        url="/"
    ),
    simple_flask_cms.dataclasses.FragmentType(
        name="Home Column 2",
        description="Displayed in the right column in the home page",
        url="/"
    )
]
simple_flask_cms.config.extra_nav_urls = [
    simple_flask_cms.dataclasses.ExtraNavUrl(
        nav_title="Color",
        path="colors",
        redirect="/colors"
    )
]

if __name__ == '__main__':
    app.run()
